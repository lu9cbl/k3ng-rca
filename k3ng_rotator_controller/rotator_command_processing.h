
/*
B              - Report elevation
C              - Report azimuth
C2             - Report azimuth and elevation
S              - Stop all rotation
A              - Stop azimuth rotation
E              - Stop elevation rotation
L              - Rotate azimuth left (CCW)
R              - Rotate azimuth right (CW)
D              - Rotate elevation down
U              - Rotate elevation up
Mxxx           - Move to azimuth
Wxxx yyy       - Move to azimuth xxxx and elevation yyy
X1             - Change to azimuth rotation speed setting 1
X2             - Change to azimuth rotation speed setting 2
X3             - Change to azimuth rotation speed setting 3
X4             - Change to azimuth rotation speed setting 4
O              - Azimuth offset calibration
F              - Azimuth full scale calibration
O2             - Elevation offset calibration
F2             - Elevation full scale calibration
P36            - Switch to 360 degree mode
P45            - Switch to 450 degree mode
Z              - Toggle north / south centered mode
H              - Help
*/
